# Rappel du projet
Le projet Gestform est un programme Python qui permet de traiter des nombres entiers et de leur attribuer 
une chaîne de caractères en fonction de certaines règles. 

- Si un nombre est divisible par 3, il sera remplacé par la chaîne "Geste". 
- S'il est divisible par 5, il sera remplacé par la chaîne "Forme". 
- S'il est divisible à la fois par 3 et par 5, il sera remplacé par la chaîne "Gestform". 
- Si un nombre ne satisfait aucune de ces conditions, il restera inchangé.

# Prérequis
- Python 3.6 ou supérieur.

# Installation

Clonez le dépôt Git sur votre machine
```bash
git clone https://gitlab.com/mikael-descoins1/gestform.git
cd gestform
```

# Architecture du projet

Le projet Gestform est organisé de la manière suivante :
```css
.
├── config
│   └── settings.py
├── src
│   └── gestform
│       ├── exceptions.py
│       └── services
│           └── number_processor_service.py
├── tests
│   └── test_number_processor.py
├── main.py
```

Le dossier ```config``` contient le fichier ```settings.py```, qui définit les 
constantes ```MIN_NUMBER``` et ```MAX_NUMBER``` (bornes maximales et minimales du projet).

Le dossier ```src``` contient le module ```gestform```, qui contient les classes ```NumberProcessorService``` 
et ```OutOfRangeError```.

Le dossier ```tests``` contient le fichier ```test_number_processor.py```, qui contient les tests unitaires 
pour le module ```NumberProcessorService```.

Le fichier ```main.py``` est le point d'entrée du programme.

# Lancer les tests

Pour lancer les tests unitaires, exécutez la commande suivante depuis le dossier racine du projet : 

Mac OS : 
```bash
python3 -m unittest tests/test_number_processor.py 
```
Windows : 
```bash
py -m unittest tests/test_number_processor.py 
```

# Lancer le projet

Pour lancer le programme, exécutez la commande suivante depuis le dossier racine du projet :

Mac OS :
```css
python3 main.py
```

Windows : 
```css
py main.py
```

Le programme va alors générer une liste de 10 nombres entiers aléatoires compris entre ```MIN_NUMBER``` 
et ```MAX_NUMBER``` définis dans le fichier ```settings.py```. 
Chaque nombre sera ensuite traité par l'instance de la classe ```NumberProcessorService```, qui renverra 
soit une chaîne de caractères, soit le nombre lui-même. 

Le résultat sera affiché dans la console pour chaque nombre traité. 
Si une exception est levée lors du traitement d'un nombre, le message d'erreur sera affiché à la place du résultat.

