from src.gestform.exceptions import OutOfRangeError
from config.settings import MIN_NUMBER, MAX_NUMBER

class NumberProcessorService:
    def __init__(self, min_number=MIN_NUMBER, max_number=MAX_NUMBER):
        self.min_number = min_number
        self.max_number = max_number

    def process_number(self, n):
        if not isinstance(n, int):
            raise TypeError(f"{n} n'est pas un entier")
        if n < self.min_number or n > self.max_number:
            raise OutOfRangeError(n, self.min_number, self.max_number)

        if n % 3 == 0 and n % 5 == 0:
            return "Gestform"
        elif n % 3 == 0:
            return "Geste"
        elif n % 5 == 0:
            return "Forme"
        else:
            return n

