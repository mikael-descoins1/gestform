class OutOfRangeError(Exception):
    def __init__(self, value, min_value, max_value):
        self.value = value
        self.min_value = min_value
        self.max_value = max_value
        super().__init__(f"Le nombre {value} est hors des limites [{min_value}, {max_value}]")
