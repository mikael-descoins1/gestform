import sys
sys.path.append("./src")
import random
from config.settings import MIN_NUMBER, MAX_NUMBER
from src.gestform.services.number_processor_service import NumberProcessorService

def main():
    # Création d'une liste aléatoire de nombres entiers
    numbers = [random.randint(MIN_NUMBER, MAX_NUMBER) for _ in range(10)]

    # Initialisation du service de traitement des nombres
    number_processor = NumberProcessorService(min_number=MIN_NUMBER, max_number=MAX_NUMBER)

    for number in numbers:
        try:
            result = number_processor.process_number(number)
            print(f"{number}: {result}")
        except Exception as e:
            print(f"{number}: {e}")

if __name__ == "__main__":
    main()