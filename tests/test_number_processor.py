import sys
sys.path.append("./src")
import unittest
from src.gestform.exceptions import OutOfRangeError
from src.gestform.services.number_processor_service import NumberProcessorService
from config.settings import MIN_NUMBER, MAX_NUMBER

class TestNumberProcessorService(unittest.TestCase):

    def setUp(self):
        self.number_processor_service = NumberProcessorService()

    def test_geste(self):
        self.assertEqual(self.number_processor_service.process_number(3), "Geste")

    def test_forme(self):
        self.assertEqual(self.number_processor_service.process_number(5), "Forme")

    def test_gestform(self):
        self.assertEqual(self.number_processor_service.process_number(15), "Gestform")

    def test_number(self):
        self.assertEqual(self.number_processor_service.process_number(1), 1)

    def test_out_of_range_low(self):
        with self.assertRaises(OutOfRangeError):
            self.number_processor_service.process_number(MIN_NUMBER - 1)

    def test_out_of_range_high(self):
        with self.assertRaises(OutOfRangeError):
            self.number_processor_service.process_number(MAX_NUMBER + 1)

    def test_non_integer_input(self):
        with self.assertRaises(TypeError):
            self.number_processor_service.process_number('invalid')
        with self.assertRaises(TypeError):
            self.number_processor_service.process_number(3.14)
        with self.assertRaises(TypeError):
            self.number_processor_service.process_number([])
        with self.assertRaises(TypeError):
            self.number_processor_service.process_number({})


if __name__ == '__main__':
    unittest.main()
