# Module gestform

Le module `gestform` contient les classes et fonctions nécessaires au traitement des nombres dans le projet **Gestform**.

## Classes
### `NumberProcessorService`
La classe `NumberProcessorService` est utilisée pour traiter des nombres et leur attribuer une chaîne de caractères 
en fonction de certaines règles. 

Elle contient les méthodes suivantes :

#### `__init__(self, min_number=MIN_NUMBER, max_number=MAX_NUMBER)`
Initialise une instance de la classe NumberProcessorService avec les bornes minimale et maximale spécifiées 
(par défaut, les bornes sont définies dans le fichier config/settings.py).

#### `process_number(self, n)`
Traite un nombre donné et renvoie une chaîne de caractères ou le nombre lui-même en fonction des règles spécifiées 
dans le projet.

Exemple d'utilisation :

```python 
processor = NumberProcessorService(min_number=1, max_number=10)
result = processor.process_number(3)
print(result)  # "Geste"
```


## Exceptions
### `OutOfRangeError`
L'exception `OutOfRangeError` est levée si le nombre passé en paramètre à la méthode `process_number` 
de la classe `NumberProcessorService` est en dehors des bornes spécifiées.

Elle contient les attributs suivants :

- `value` : le nombre qui a provoqué l'erreur.
- `min_value` : la borne minimale spécifiée.
- `max_value` : la borne maximale spécifiée.

Exemple d'utilisation :

```python
processor = NumberProcessorService(min_number=1, max_number=10)
try:
    processor.process_number(20)
except OutOfRangeError as e:
    print(e)  # "Le nombre 20 est hors des limites [1, 10]"
```